# Kiwi - little wiki

Kiwi is a simplistic small wiki written in python.

It respects [Wiki principles](http://wiki.c2.com/?WikiPrinciples).
