import json, os, re, time
from pathlib import Path

import markdown

from flask import Flask, g, redirect, render_template, request, session, url_for
from flask.cli import load_dotenv

load_dotenv()
application = Flask(__name__)
application.config.from_prefixed_env()
if not os.path.exists("users.json"):
    with open("users.json", "w") as file_handle:
        file_handle.write("{}")

PAGE = r"(([A-ZÀ-Ö][a-zØ-öø-ÿ]+){2,})"
AUTOLINK = r"((?<=[^A-ZÀ-Öa-zØ-öø-ÿ/\"\(])|^)(([A-ZÀ-Ö][a-zØ-öø-ÿ]+){2,})((?=\W)|$)"
HTMLLINK = r'([^"])(https?://[^\s<]+)'
DATAFOLDER = Path(os.environ.get("DATAS", "datas/"))


@application.route("/robots.txt")
def robots():
    return "User-agent: *\nDisallow: /"


@application.route("/login", methods=["GET", "POST"])
def login():
    with open("users.json") as file_handle:
        users = json.load(file_handle)

    if request.method == "POST":
        login = request.form["login"]
        if login in users and request.form["password"] == users[login]:
            session["user"] = login
            return redirect(url_for("index"))

    return render_template("login.html")


@application.route("/logout")
def logout():
    session.pop("user", None)
    return redirect(url_for("index"))


@application.route("/now")
def now():
    return redirect(url_for("index", page="NowPage"))


@application.route("/")
@application.route("/<page>")
def index(page="HomePage"):
    g.page = page
    if not re.match(PAGE, page):
        return redirect(url_for("index"))
    g.user = session.get("user", None)
    file_path = DATAFOLDER / page

    # Retrieving backlinks
    try:
        with open(DATAFOLDER / "backlinks.json") as file_handle:
            backlinks = json.load(file_handle)
    except:
        backlinks = {}
    g.backlinks = backlinks.get(page, [])

    # Loading page content
    try:
        with open(file_path) as file_handle:
            source = file_handle.read()
        g.last_modification = time.strftime(
            "%Y-%m-%d %H:%M",
            time.gmtime(os.path.getmtime("users.json"))
        )
    except:
        source = "You came to an empty page."
        g.backlinks = []
        g.last_modification = None

    g.content = source
    # Conversion to HTML
    g.content = markdown.markdown(g.content)
    # Convert simple links into HTML links
    g.content = re.sub(HTMLLINK, r'\1<a href="\2">\2</a>', g.content)
    # Automatic Internal Link
    g.content = re.sub(AUTOLINK, r'<a href="/\2">\2</a>', g.content, flags=re.M)

    return render_template("page.html")


@application.route("/<page>/edit", methods=["GET", "POST"])
def edit(page):
    if session.get("user", None) is None:
        return redirect(url_for("index"))

    g.page = page
    if not re.match(PAGE, page):
        return redirect(url_for("index"))
    # Load known backlinks
    file_path = DATAFOLDER / page
    try:
        with open(DATAFOLDER / "backlinks.json") as file_handle:
            backlinks = json.load(file_handle)
    except:
        backlinks = {}

    # Loading page content
    try:
        with open(file_path) as file_handle:
            g.source = file_handle.read()
    except:
        g.source = ""

    # Updating page source if necessary
    if request.method == "POST":
        source = request.form["source"]
        if source.strip() == "":
            # Deleting page
            os.unlink(file_path)
        else:
            with open(file_path, "w") as file_handle:
                file_handle.write(source)

        # Updating Backlinks
        for backlink_page in backlinks:
            backlinks[backlink_page] = [link for link in backlinks[backlink_page] if link != page]
        # Adding backlinks in pages linked
        for backlink in re.findall(AUTOLINK, source, flags=re.M):
            if page == backlink[0]:
                continue
            if backlink[0] not in backlinks:
                backlinks[backlink[0]] = [page]
            elif page not in backlinks[backlink[0]]:
                backlinks[backlink[0]].append(page)
        with open(DATAFOLDER / "backlinks.json", "w") as file_handle:
            json.dump(backlinks, file_handle, indent=2)

        return redirect(url_for("index", page=page))

    return render_template("edit.html")

